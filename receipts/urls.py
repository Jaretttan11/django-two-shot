from django.urls import path
from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    AccountReceiptCreateView,
    CategoryCreateView,
    AccountListView,
    ExpenseCategoryListView
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
    path(
        "accounts/create/",
        AccountReceiptCreateView.as_view(),
        name="account_create",
    ),
    path(
        "catergories/create/",
        CategoryCreateView.as_view(),
        name="category_create",
    ),
    path("accounts/", AccountListView.as_view(), name="accounts_list"),
    path("categories/", ExpenseCategoryListView.as_view(), name="categories_list")
]
